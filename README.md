# aquagpusph-installable_tool_demo

This is just a tiny demo to show how new installable tools works in AQUAgpusph.

# How to

## Compile AQUAgpusph 2-D

Herein we are not installing AQUAgpusph, but compiling it right in
`$HOME/aquagpusph` folder. Indeed you can type

```
cd $HOME
git clone https://gitlab.com/sanguinariojoe/aquagpusph.git
cd aquagpusph
cmake -DAQUAGPUSPH_3D=OFF -DCMAKE_BUILD_TYPE=Release .
make
```

## Compile the installable tool

Let's download the demo in `$HOME/aquagpusph-installable_tool_demo` folder:

```
cd $HOME
git clone https://gitlab.com/sanguinariojoe/aquagpusph-installable_tool_demo.git
cd aquagpusph-installable_tool_demo
```

To compile the installable tool library, we can just use the makefile, feeding
it with the folder of the AQUAgpusph header files, i.e.

```
make AQUAGPUSPH_INCLUDE=$HOME/aquagpusph
```

Notice that Makefile is looking for headers both in `$(AQUAGPUSPH_INCLUDE)` and
its `$(AQUAGPUSPH_INCLUDE)/include` subfolder.

## Launch the example

Now the example can be launched typing

```
cd $HOME/aquagpusph-installable_tool_demo
$HOME/aquagpusph/bin/AQUAgpusph2D -i Main.xml
```

If everything worked fine, you should see the following message repeated at the
screen:

```
(InstallableDemo::_execute): Executing Installed demo tool!
```

You can stop AQUAgpusph pressing `c` key.
